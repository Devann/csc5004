package org.example.abd;

import org.example.abd.quorum.Majority;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.example.abd.cmd.Command;
import org.example.abd.cmd.CommandFactory;
import org.example.abd.cmd.ReadReply;
import org.example.abd.cmd.ReadRequest;
import org.example.abd.cmd.WriteReply;
import org.example.abd.cmd.WriteRequest;
import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;

public class RegisterImpl<V> extends ReceiverAdapter implements Register<V> {

    private int value;
    private int label;
    private int max;
    private boolean isWritable;
    private Majority quorumSystem;

    private String name;
    private CommandFactory<V> factory;
    private JChannel channel;

    public RegisterImpl(String name) {
        this.name = name;
        this.factory = new CommandFactory<>();
    }

    public void open(boolean isWritable) throws Exception {
        this.value = 0;
        this.label = 0;
        this.max = 0;
        this.isWritable = isWritable;
        channel = new JChannel();
        channel.connect(name);
        channel.setReceiver(this);
    }

    @Override
    public void viewAccepted(View view) {
        quorumSystem = new Majority(view);
    }

    // Client part

    @Override
    public void open() {
    }

    @Override
    public V read() {
        ReadRequest<V> cmd = this.factory.newReadRequest();
        return execute(cmd);
    }

    @Override
    public void write(V v) {
        this.label = this.label + this.max;
        WriteRequest<V> cmd = this.factory.newWriteRequest(v, label);
        execute(cmd);
    }

    @Override
    public void close() {
    }

    private synchronized V execute(Command cmd) {
        CompletableFuture<V> result = new CompletableFuture<>();
        List<Address> myQuorums = quorumSystem.pickQuorum();

        if (((cmd instanceof WriteReply) || (cmd instanceof WriteRequest)) && (!isWritable)) {
            throw new IllegalArgumentException();
        }

        if (cmd instanceof WriteRequest) {
            for (Address myAddress : myQuorums) {
                send(myAddress, cmd);
            }
        } else if (cmd instanceof WriteReply) {
            V res = null;
            try {
                res = result.get();
            } catch (Exception e) {
                //
            }
        }

        return res;
    }

    // Message handlers

    @Override
    public void receive(Message msg) {
        Address src = msg.getSrc();
        Command cmd = (Command) msg.getObject();

        if (cmd instanceof WriteRequest) {
            int l = cmd.getTag();
            int cmd_value = (int) cmd.getValue();
            if (l > this.label) {
                this.label = l;
                this.value = cmd_value;
            }
            WriteReply reply = this.factory.newWriteReply();
            send(src, reply);
        }

        if (cmd instanceof ReadRequest) {
            ReadReply<V> reply = this.factory.newReadReply(this.label, this.value);
            send(src, reply);
        }

    }

    private void send(Address dst, Command command) {
        try {
            Message message = new Message(dst, channel.getAddress(), command);
            channel.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
