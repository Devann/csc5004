package org.example.abd.quorum;

import org.jgroups.Address;
import org.jgroups.View;
import static java.lang.Math.ceil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Majority {
    private View view;

    public Majority(View view) {
        this.view = view;
    }

    public int quorumSize() {
        return (int) ceil(view.getMembers().size() / 2 + 1);
    }

    public List<Address> pickQuorum() {
        int taille = quorumSize();
        int nb_membres = view.getMembers().size();

        List<Address> allElements = view.getMembers();
        List<Address> selectedElements = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < taille; i++) {
            int randomIndex = random.nextInt(nb_membres);
            Address randomAddress = allElements.get(randomIndex);
            allElements.remove(randomAddress);
            selectedElements.add(randomAddress);
        }

        return selectedElements;
    }

}