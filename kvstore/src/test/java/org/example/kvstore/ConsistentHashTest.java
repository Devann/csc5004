package org.example.kvstore;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.example.kvstore.distribution.ConsistentHash;
import org.jgroups.Address;
import org.jgroups.View;
import org.jgroups.util.UUID;
import org.junit.Test;

public class ConsistentHashTest {
	
	  @Test
	    public void testStringAddresses() {
	        Address address1 = new UUID();
	        Address address2 = new UUID();
	        List<Address> addressesList=new ArrayList<>();
	        addressesList.add(address1);
	        addressesList.add(address2);

	        assertNotNull(address1);
	        assertNotNull(address2);

	        View view= new View(address1,1,addressesList);
	        ConsistentHash consistentHash = new ConsistentHash(view);
	        String testString = "GIZZGAGZGZEZEA";
	        Address address = consistentHash.lookup(testString);
	        assertNotNull(address);
	    }
}