package org.example.kvstore.distribution;

import org.jgroups.Address;
import org.jgroups.View;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class ConsistentHash implements Strategy{

    private TreeSet<Integer> ring;
    private Map<Integer,Address> addresses;

    public ConsistentHash(View view){
    	List<Address> members= view.getMembers();
    	ring=new TreeSet<Integer>();
    	addresses=new HashMap<Integer,Address>();
    	for(Address a:members) {
    		int hash=a.hashCode();
    		ring.add(hash);
    		addresses.put(hash, a);
    	}
    }

    @Override
    public Address lookup(Object key){
    	int localHash = key.hashCode();
    	int nodeHash = -1;
    	if (localHash > ring.last()) {
    		nodeHash = ring.first();
    	} else  {
    		nodeHash = ring.ceiling(key.hashCode());
    	}
    	return addresses.get(nodeHash);
    }
}
